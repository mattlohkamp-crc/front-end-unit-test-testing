import fetch from 'node-fetch';
export default function getAllCameras(){
	return fetch('https://dev.irisx.org/api/cameras?cameraTypes=CCTV').then(data=>data.json()).then(cameras=>cameras.map(camera=>camera.name));
}