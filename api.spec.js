import chaiAsPromised from 'chai-as-promised';
import {expect, use } from 'chai';
import { describe, it } from 'mocha';
import getAllCameras from './api';

use(chaiAsPromised);

describe.only('APITest', ()=>{
	it('cameras api should return some cameras',()=>{
		return expect(getAllCameras()).to.eventually.be.an('array').that.is.not.empty;
	})
})