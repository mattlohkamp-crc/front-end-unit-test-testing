import chaiAsPromised from 'chai-as-promised';
import {expect, use } from 'chai';
import { describe, it } from 'mocha';

use(chaiAsPromised);

function someMadeUpAyncFunc(boolValue, cb) {
  return new Promise(function(resolve){
    setTimeout(function() {
      resolve(boolValue ? "You get a sweet :)" : "You get nothing!!")
    }, 1500);
  })
}

describe("AsyncTest", function()  {
  it("should return `You get a sweet :)` if `true` is passed in", function() {
    return expect(someMadeUpAyncFunc(true)).to.eventually.equal("You get a sweet :)");
  });

  it("should return `You get nothing!!` if `false` is passed in", function() {
    return expect(someMadeUpAyncFunc(false)).to.eventually.equal("You get nothing!!");
  });
});