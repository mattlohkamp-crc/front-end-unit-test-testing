/**
 * @function gridIndexToXYCoords
 * @description Assuming cells in a square grid are numbered left-right-right, top-to-bottom,
 * with the top left being '0' - takes such an index and the size of the grid, and return the
 * X and Y coordinates of that cell.
 * @static
 * @global
 * @param {number} index Integer, the index of a cell in a square grid.
 * @param {number} gridSize An integer, the size of one dimension of a square grid.
 * @returns {Point}
 *
 * @example
 *
 * gridIndexToXYCoords(0, 1); // the first cell in a 1x1 grid -> {x:0,y:0}, aka upper left corner
 * gridIndexToXYCoords(5, 3); // the sixth cell in a 3x3 grid -> {x:2,y:1}, aka middle right cell
 * // cell 5, in a 3x3 grid:
 * // [0][1][2]
 * // [3][4][5] <--- 2,1
 * // [6][7][8]
 */
export function gridIndexToXYCoords(index, gridSize) {
	const x = index % gridSize;
	const y = Math.floor(index / gridSize);
	return { x, y };
}

/**
 * @function gridXYCoordsToIndex
 * @description Assuming cells in a square grid are numbered left-right-right, top-to-bottom,
 * with the top left being '0' - takes the X and Y coordinates to such a cell and the size of
 * the grid, and return the index of that cell.
 * @static
 * @global
 * @param {Point} XY A pair of integers, the X and Y coordinates of a cell in a square grid.
 * @param {number} gridSize An integer, the size of one dimension of a square grid.
 * @returns {number|boolean}
 *
 * @example
 * gridXYCoordsToIndex({ x: 0, y: 0 }, 1); // upper left corner in a 1x1 grid -> 0, aka first cell
 * gridXYCoordsToIndex({ x: 2, y: 1 }, 3); // middle right cell in a 3x3 grid -> 5, aka sixth cell
 * // cell 2,1 in a 3x3 grid:
 * // [0][1][2]
 * // [3][4][5] <--- 5
 * // [6][7][8]
 */
export function gridXYCoordsToIndex({ x, y }, gridSize) {
	if (x < gridSize && y < gridSize) {
		return y * gridSize + x;
	}
	return false;
}