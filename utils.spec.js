import { expect } from 'chai';
import {
  gridIndexToXYCoords,
  gridXYCoordsToIndex,
} from './utils.js';

describe('gridIndexToXYCoords', () => {
  /*
    [0, 1, 2
     3, 4, 5
     5, 6, 7]
  */

  it('should determine grid coords from index', () => {
    const coordinates = gridIndexToXYCoords(5, 3);
    expect(coordinates.x).to.equal(2);
    expect(coordinates.y).to.equal(1);
  });
});


describe('gridXYCoordsToIndex', () => {
  /*
    [0, 1, 2
     3, 4, 5
     5, 6, 7]
  */

  it('should determine index from grid coords', () => {
    const coordinates = { x: 2, y: 1 };
    let index = gridXYCoordsToIndex(coordinates, 3);
    expect(index).to.equal(5);
  })

} );